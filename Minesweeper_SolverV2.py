"""
Minesweeper_SolverV2
"""

import sys
import requests
import random
import re
import openpyxl
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import numpy as np

if sys.platform == "win32":
    driver = webdriver.Chrome('/Users/username/chromedriver')
    #for running consecutively and automatically tabulating results in .xslx
    file = '/Users/username/minesweeper_solver_results.xlsx'
elif sys.platform == "darwin":
    driver = webdriver.Chrome('/Users/username/chromedriver')
    # for running consecutively and automatically tabulating results in .xslx
    file = '/Users/username/minesweeper_solver_results.xlsx'
else:
    sys.exit('OS not recognized.')

#determining results
num_attempts = 0
wb = openpyxl.load_workbook(file)
num_sheet = len(wb.worksheets) + 1
sheet = wb.create_sheet(f'#{num_sheet}', 0)
wb.save(file)

#starting driver and url
url = 'http://minesweeperonline.com/'
driver.get(url)
input("Manually, set the game mode and then click 'enter' when ready.")
html = driver.page_source
soup = BeautifulSoup(html, 'lxml')

#regular expressions needed to find the coordinates, row/column numbers, and square values
regex_1 = re.compile(r'\d+_\d+')
regex_r = re.compile(r'(\d+)_')
regex_c = re.compile(r'_(\d+)')
regex_square_value =  re.compile(r'open(\d)')

#determining the amount of squares/columns/rows based on the border
column_amount = int(len(soup.find_all(class_='bordertb'))/3)
row_amount = int(len(soup.find_all(class_='borderlr'))/2)

#create initial grid/array of the minesweeper field
html = driver.page_source
soup = BeautifulSoup(html, 'lxml')
grid = [['-' for c in range(column_amount)] for r in range(row_amount)]

#determining the amount of mines
mines_hundreds_java = str(soup.find_all(id='mines_hundreds'))
mines_hundreds = ''.join(i for i in mines_hundreds_java if i.isdigit())
mines_tens_java = str(soup.find_all(id='mines_tens'))
mines_tens = ''.join(i for i in mines_tens_java if i.isdigit())
mines_ones_java = str(soup.find_all(id='mines_ones'))
mines_ones = ''.join(i for i in mines_ones_java if i.isdigit())
mines_amount = int(mines_hundreds + mines_tens + mines_ones)

#creating a list of all possible squares minus the ones that are invisible and unclickable around the border
    #this is used in the adjacent function
all_squares_soup = soup.find_all(class_='square blank')
unseen_squares_soup = (soup.find_all(class_='square blank', style='display: none;'))
all_squares = []
unseen_squares = []
possible_squares = []
for i in unseen_squares_soup:
    unseen_squares.append(str(i))
for i in all_squares_soup:
    all_squares.append(str(i))
for i in all_squares:
    if i not in unseen_squares:
        possible_squares.append(str(i))
    else:
        continue

def random_start():
    global unsolved_squares_coords
    # unsolved_squares_coords --> creating a list of all squares that haven't been 'solved' - aka have 'blanks' within their adjacent lists still
    unsolved_squares_coords = [f'{r + 1}_{c + 1}' for r in range(row_amount) for c in range(column_amount)]
    random_coords = random.choice(unsolved_squares_coords)
    driver.find_element_by_id(f'{random_coords}').click()

def record(result):
    global num_attempts
    global time_remaining
    global num_sheet
    global file
    html = driver.page_source
    soup = BeautifulSoup(html, 'lxml')
    wb = openpyxl.load_workbook(file)
    sheet = wb.active
    sheet['A1'].value = 'Attempts'
    sheet['B1'].value = 'Result'
    sheet['C1'].value = 'Time'
    sheet['D1'].value = 'Bombs Remaining'
    num_attempts += 1
    sheet[f'A{num_attempts + 1}'].value = str(num_attempts)
    if result == 'loss':
        sheet[f'B{num_attempts + 1}'].value = 'loss'
    if result == 'win':
        sheet[f'B{num_attempts + 1}'].value = 'win'
    secs_hundreds_java = str(soup.find_all(id='seconds_hundreds'))
    secs_hundreds = ''.join(i for i in secs_hundreds_java if i.isdigit())
    secs_tens_java = str(soup.find_all(id='seconds_tens'))
    secs_tens = ''.join(i for i in secs_tens_java if i.isdigit())
    secs_ones_java = str(soup.find_all(id='seconds_ones'))
    secs_ones = ''.join(i for i in secs_ones_java if i.isdigit())
    secs_amount = int(secs_hundreds + secs_tens + secs_ones)
    sheet[f'C{num_attempts + 1}'].value = str(secs_amount)
    mines_hundreds_java = str(soup.find_all(id='mines_hundreds'))
    mines_hundreds = ''.join(i for i in mines_hundreds_java if i.isdigit())
    mines_tens_java = str(soup.find_all(id='mines_tens'))
    mines_tens = ''.join(i for i in mines_tens_java if i.isdigit())
    mines_ones_java = str(soup.find_all(id='mines_ones'))
    mines_ones = ''.join(i for i in mines_ones_java if i.isdigit())
    mines_amount = int(mines_hundreds + mines_tens + mines_ones)
    sheet[f'D{num_attempts + 1}'].value = str(mines_amount)
    wb.save(file)

def restart():
    html = driver.page_source
    soup = BeautifulSoup(html, 'lxml')
    driver.find_element_by_id('face').click()
    update_grid()
    random_start()
    search_flag_clearV2()

def check_result():
    html = driver.page_source
    soup = BeautifulSoup(html, 'lxml')
    face = str(soup.find_all(id='face'))
    if 'facedead' in face:
        print('loss')
        record(result='loss')
        restart()
    if 'facewin' in face:
        print('win')
        record(result='win')
        restart()

def update_grid():
    global grid
    html = driver.page_source
    soup = BeautifulSoup(html, 'lxml')
    for r in range(row_amount):
        for c in range(column_amount):
            grid[r][c] = str(soup.find(id=f'{r+1}_{c+1}'))

"""defining adjacent squares
the ids are written as row_column
basically adjacent squares are +/- 1 from the same row and then +1, -1, or == to on the +/- 1 rows"""
def adjacent(square_coords):
    html = driver.page_source
    soup = BeautifulSoup(html, 'lxml')
    r_list = regex_r.findall(square_coords)
    r = int(r_list[0])
    c_list = regex_c.findall(square_coords)
    c = int(c_list[0])
    adjacent_squares_soup = []
    adjacent_squares = []
    adjacent_squares_soup.append(str(soup.find(id=f'{r - 1}_{c - 1}'))) #defining maxes for the adjacents, or maybe just saying if it doesn't exist, don't worry about it.
    adjacent_squares_soup.append(str(soup.find(id=f'{r - 1}_{c}')))
    adjacent_squares_soup.append(str(soup.find(id=f'{r - 1}_{c + 1}')))
    adjacent_squares_soup.append(str(soup.find(id=f'{r}_{c + 1}')))
    adjacent_squares_soup.append(str(soup.find(id=f'{r + 1}_{c + 1}')))
    adjacent_squares_soup.append(str(soup.find(id=f'{r + 1}_{c}')))
    adjacent_squares_soup.append(str(soup.find(id=f'{r + 1}_{c - 1}')))
    adjacent_squares_soup.append(str(soup.find(id=f'{r}_{c - 1}')))
    for i in adjacent_squares_soup:
        if i not in unseen_squares:
            adjacent_squares.append(i)
        else:
            continue
    return adjacent_squares

def right_click(x): #sets bombflag
    bomb_r_list = regex_r.findall(x)
    bomb_r = int(bomb_r_list[0])
    bomb_c_list = regex_c.findall(x)
    bomb_c = int(bomb_c_list[0])
    action = ActionChains(driver)
    bomb_square = driver.find_element_by_id(f'{int(bomb_r)}_{int(bomb_c)}')
    action.context_click(bomb_square).perform()

def left_click(x): #clears square
    bf_r_list = regex_r.findall(x)
    bf_r = int(bf_r_list[0])
    bf_c_list = regex_c.findall(x)
    bf_c = int(bf_c_list[0])
    driver.find_element_by_id(f'{int(bf_r)}_{int(bf_c)}').click()

def simple_sweep(square_coords):
    num_bombs = int(regex_square_value.findall(square_coords)[0])
    x = regex_1.findall(square_coords)[0]
    adjacent_blanks = []
    adjacent_bombflags = []
    adjacent_squares = adjacent(x)
    for i in adjacent_squares:
        if 'bombflagged' in i:
            adjacent_bombflags.append(i)
            continue
        if 'blank' in i:
            adjacent_blanks.append(i)
            continue
        else:
            continue
    if len(adjacent_bombflags) == num_bombs and len(adjacent_blanks) >= 1:
        for j in adjacent_blanks:
            if 'blank' not in j: #keeping this in here so that if clearing one bomb creates open0, don't get the "can't click" error
                continue
            else:
                left_click(j)
                continue
        update_grid()
    if len(adjacent_blanks) + len(adjacent_bombflags) == num_bombs and len(adjacent_blanks) >= 1:
        for k in adjacent_blanks:
            right_click(k)
        update_grid()
    else:
        advanced_sweep(square_coords)

def search_flag_clearV2():
    global grid
    global unsolved_squares_coords
    update_grid()
    unsolved_squares_coords_TEMP = unsolved_squares_coords[:]
    print('unsolved spaces: ' + str(len(unsolved_squares_coords))) #can remove this, but useful to see if/when the program gets stuck and needs to guess
    amount_unsolved = len(unsolved_squares_coords)
    for i in unsolved_squares_coords:
        r = int(regex_r.findall(i)[0])
        c = int(regex_c.findall(i)[0])
        if 'blank' in grid[r-1][c-1]:
            continue
        if 'bombflagged' in grid[r-1][c-1]:
            unsolved_squares_coords_TEMP.remove(i)
            continue
        if 'open0' in grid[r-1][c-1]:
            unsolved_squares_coords_TEMP.remove(i)
            continue
        if 'open' in grid[r-1][c-1]:
            adjacent_squares = adjacent(i)
            z = None
            for j in adjacent_squares:
                if 'blank' in j:
                    z = False
                    break
                else:
                    z = True
                    continue
            if z == False:
                html = driver.page_source
                soup = BeautifulSoup(html, 'lxml')
                x = str(soup.find(id=f'{r}_{c}'))
                simple_sweep(x)
            if z == True:
                unsolved_squares_coords_TEMP.remove(i)
    unsolved_squares_coords = unsolved_squares_coords_TEMP
    if amount_unsolved == len(unsolved_squares_coords): #ensuring that it doesn't just get stuck either no bombs left, but still blanks or with 50/50 guess situations
        check_result()
        if mines_amount == 0:
            print('All bombs accounted for. Clearing the rest of the field.')
            update_grid()
            unsolved_blanks = []
            for i in unsolved_squares_coords:
                r = int(regex_r.findall(i)[0])
                c = int(regex_c.findall(i)[0])
                if 'blank' in grid[r - 1][c - 1]:
                    unsolved_blanks.append(i)
            for i in unsolved_blanks:
                left_click(i)
        else:
            print('Stuck, so guessing...','')
            random_guess()
            check_result()
    search_flag_clearV2()

def blanks_completely_overlap(adjacent_blanks, adv_adjacent_blanks):
    x=None
    for i in adjacent_blanks:
        if i in adv_adjacent_blanks:
            x=True
            continue
        else:
            x=False
            break
    return x

def overlap_and_nonoverlap_spaces(adjacent_blanks, adv_adjacent_blanks):
    overlap_spaces = []
    nonoverlap_spaces = []
    adv_nonoverlap_spaces = []
    for i in adjacent_blanks:
        if i not in adv_adjacent_blanks:
            nonoverlap_spaces.append(i)
        else:
            overlap_spaces.append(i)
    for i in adv_adjacent_blanks:
        if i not in adjacent_blanks:
            adv_nonoverlap_spaces.append(i)
        else:
            pass
    return overlap_spaces, nonoverlap_spaces, adv_nonoverlap_spaces

def advanced_sweep(square_coords):
    num_bombs = int(regex_square_value.findall(square_coords)[0])
    x = regex_1.findall(square_coords)[0]
    adjacent_blanks = []
    adjacent_bombflags = []
    adjacent_squares = adjacent(x)
    for i in adjacent_squares:
        if 'bombflagged' in i:
            adjacent_bombflags.append(i)
        if 'blank' in i:
            adjacent_blanks.append(i)
    for i in adjacent_squares:
        if i not in adjacent_bombflags and i not in adjacent_blanks and 'open0' not in i:
            adv_adjacent_squares = adjacent(i)
            adv_adjacent_blanks = []
            adv_adjacent_bombflags = []
            adv_num_bombs = int(regex_square_value.findall(i)[0])
            for j in adv_adjacent_squares:
                if 'bombflagged' in j:
                    adv_adjacent_bombflags.append(j)
                if 'blank' in j:
                    adv_adjacent_blanks.append(j)
            if blanks_completely_overlap(adjacent_blanks, adv_adjacent_blanks) == True:
                if (adv_num_bombs-len(adv_adjacent_bombflags))-(num_bombs-len(adjacent_bombflags)) == 0:
                    #aka if all the bombs are contained within the overlap
                    for k in adv_adjacent_blanks:
                        if k not in adjacent_blanks:
                            left_click(k)
                    update_grid()
                if (adv_num_bombs-len(adv_adjacent_bombflags))-(num_bombs-len(adjacent_bombflags)) >= 1:
                    if len(adv_adjacent_blanks)-len(adjacent_blanks) == ((adv_num_bombs-len(adv_adjacent_bombflags))-(num_bombs-len(adjacent_bombflags))):
                        #aka if there are some bombs leftover in the bigger list AND the leftover blanks in that list equal the amt of bombs left
                        for k in adv_adjacent_blanks:
                            if k not in adjacent_blanks:
                                right_click(k)
                        update_grid()
                else:
                    pass
            else:
                #partial overlap situations
                overlap_spaces, nonoverlap_spaces, adv_nonoverlap_spaces = overlap_and_nonoverlap_spaces(adjacent_blanks, adv_adjacent_blanks)
                """
                1) if (adv_adjacent space amt bombs needed) > (its amt of non-overlapping spaces), we can determine the minimum 
                amt of bombs that will exist within the overlap. If that number is equal to the num_bombs needed for the 
                original adjacent space, we can clear (left-click) all of its non-overlapping spaces.
                """
                if (adv_num_bombs-len(adv_adjacent_bombflags)) > len(adv_nonoverlap_spaces):
                    min_num_bomb_in_overlap = ((adv_num_bombs-len(adv_adjacent_bombflags)) - len(adv_nonoverlap_spaces))
                    if (num_bombs-len(adjacent_bombflags)) == min_num_bomb_in_overlap:
                        for k in nonoverlap_spaces:
                            left_click(k)
                        update_grid()

def random_guess():
    global unsolved_squares_coords
    global grid
    update_grid()
    unsolved_blanks = []
    for i in unsolved_squares_coords:
        r = int(regex_r.findall(i)[0])
        c = int(regex_c.findall(i)[0])
        if 'blank' in grid[r-1][c-1]:
            unsolved_blanks.append(i)
    random_coords = random.choice(unsolved_blanks)
    print(random_coords)
    driver.find_element_by_id(f'{random_coords}').click()

def start_minesweeper_solver():
    update_grid()
    random_start()
    search_flag_clearV2()

start_minesweeper_solver()

"""Possible To-do list:
At its current state, the program is functional, and created to try to find a middle ground between speed and accuracy. Below are listed some potential changes that I could make to optimize for more accuracy or more speed.

- end game advanced scenarios:
1) spaces that overlap, but aren't adjacent (see scenario below)
[2][-][1][1][1] The blank on the top row can be cleared because of the adjacents overlap between the 3 and the 1 in that middle column.
[F][-][-][1][-] The 3 (and the 4) both need one more bomb, so either of those spaces above it are bombs. Since that's the case, the 
[F][4][3][4][3] blank on the top row can be cleared. 
[1][2][F][F][F]

This is not something the program is set up to do well because of the way that spaces are compared to each other. I would be able to
write an end-game scenario program though (because I wouldn't want this to waste time running all game). I would have to have a 3rd
category of adjacents (for consistency - adv_adv_adjacents), where I examined the adjacents of the adv_adjacents. I would be able to
look for overlap with the original square. I would need to set up a condition where it only examined adv_adv_adjacents if they were
within the unsolved square. I don't plan on doing this right now because this program would have to run everytime it got stuck, which
could be several times per game and I think it would drastically slow things down. I'd rather the program just guess in those rare
situations, but if I ever wanted to prioritize win percentage over time, I should add this.

- unusual errors:
1) During testing, I was running the program in beginner mode. It happened to beat the game in 9 seconds and then a pop-up apparently
appeared for me to be able to input a high score. Here is the error that the program spit out: 
"selenium.common.exceptions.UnexpectedAlertPresentException: Alert Text: Please enter your name to submit your score (9)
Message: unexpected alert open: {Alert text : Please enter your name to submit your score (9)}"
I don't plan on running it in beginner mode and the typical high scores are so much faster than what the program would manage to do
that I don't feel like adding anything. If I ever wanted to run it in beginner mode and step away to see if it could get me on the 
leaderboards with a very lucky 1 or 2 second run, I would need to add a try/except line with this exception and have it input a name."""
